class Hello{
/**
this progmram is for hello world print in java.
*/
public static void main(String[] a){
System.out.printf("Hello World\n");
}
}
/**
Here is another class AnotherHello.
*/

class AnotherHello{
/**
this program used Sring arguments to print hello world in java and also at runtime.
*/
public static void main(String[] a){
System.out.println("Hello World!");
for(int i=0;i<a.length;i++){
System.out.println("Welcome to the java world "+a[i]);
}

}
}
