public class Rectangle{
	int length;//Instance variable
	int width;
	
	int area(){ //area method
	return length*width;
	}
	
	
					
	void setdimension(int l,int w){ //this is method for two arguments
	length=l;
	width=w;
	}
	
	void setdimension(int l){ 	//this method for one arguments and it is also method overloading here
		setdimension(l,l);
	}

	Rectangle(int l,int w){		//this is constructor and passes two arguments and it is use for initialize  variables consructor-1
	setdimension(l,w);
	}
		

	Rectangle(int l){		//this is constructor and here constructor overloading consructor-2
	this(l,l);
	}
	
	

}
public class Test{
	public static void main(String[] args){

	Rectangle r1,r2;
	r1=new Rectangle(7,5); //consructor-1
	System.out.println("Rectangle area is :"+r1.area());
	r1=new Rectangle(7); //consructor-2
	System.out.println("Rectangle(square) area is :"+r1.area());
}

}
