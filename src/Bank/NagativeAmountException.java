package Bank;



public class NagativeAmountException extends Exception{
private double amount;
private Account account;


	NagativeAmountException(String msg,double amt,Account ac){
		super(msg);
		this.amount=amt;
		this.account=ac;
	}


	public double getAmount(){
	return amount;	
	}
	
	
	public Account getAccount(){
	return account;
	}


	public String toString(){
	
	return super.toString()+":"+amount+":"+account;
	}
}


