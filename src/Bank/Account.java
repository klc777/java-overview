package Bank;

import Bank.*;

public  class Account{


	int account_number;
	String name;
	double balance;

	public Account(int a,String b,double c)throws NagativeAmountException{//initializing the constructor
	if(c <0){
	throw new NagativeAmountException("Neagative Deposite",c,this);	
	}
	this.account_number=a;
	this.name=b;
	this.balance=c;

	}

	int getAccountnumber(){		//method get qaccount number

	return this.account_number;
	}
	
	String getname(){	//method get account name(holder)

	return this.name;	
	}
	
	double getbalance(){	//method get balance which is in the account  

	return this.balance;
	}


	public double deposite(double amt)throws NagativeAmountException{
	if(amt <0){
	throw new NagativeAmountException("Neagative Deposite",amt,this);	
	}
		
	System.out.println("Enter the ammount which you want to deposite into your account : "+amt);
	return this.balance+=amt;
	}
	
	public boolean withdraw(double amt)throws NagativeAmountException{
	if(amt <0){
	throw new NagativeAmountException("Neagative Deposite",amt,this);	
	}
	if(this.balance<amt){
	return false;
	}
	else{
	System.out.println("Enter the ammount which you want to withdraw from your account : "+amt);
	this.balance-=amt;
	return true;
	}
	}
	
	public void display(){

	System.out.println("account :"+this.account_number+",name : "+this.name+",balance : "+this.balance);
	}

	static int lastAccountNumber=1000;
	public Account(String b,double c)throws NagativeAmountException{
	this(++lastAccountNumber,b,c);
	}

}
/**class Test{

public static void main(String[] args){

Account a=new Account(123456,"kunal",500000);

a.display();
a.deposite(1000000);
a.display();
a.withdraw(100000);
a.display();

a=new Account("kunal",500000);
a.display();


}


}*/
